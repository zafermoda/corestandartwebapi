﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStandartWebapi.Data;
using CoreStandartWebapi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreStandartWebapi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly AppDbContext context;// DbContext tipinde bir değişken tanımlıyoruz. Veritabanımız olacak.

        public ValuesController(AppDbContext context)
        {
            this.context = context;// DbContext tipindeki değişkenimize AppDbContextimize set ediyoruz.
        }        

        // GET api/values
        [HttpGet]
        public List<Category> Get()
        {
            return context.Categories.ToList();// Tüm kategorileri listeler
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Category Get(int id)
        {
            return context.Categories.Find(id);// Id si verilen kategoriyi gösterir
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Category entity)
        {
            context.Categories.Add(entity);// kategoriyi veritabanına kaydetmek için hazırlar
            context.SaveChanges();// hazırlanan veriyi veritabanına kaydeder.
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Category entity)
        {
            context.Categories.Update(entity);// kategoriyi günceller.
            context.SaveChanges();// hazırlanan veriye gerekli işlemi yapar
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var category = context.Categories.Find(id);// gelen id ye sahip kategoriyi category değişkenine atar
            context.Categories.Remove(category);// category değişkenine atanan kategoriy veritabanından siler
        }
    }
}
