using System.IO;
using CoreStandartWebapi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CoreStandartWebApi
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            //var interconnecting = configuration.GetConnectionString("DefaultConnection");
            //builder.UseSqlServer(interconnecting);
            var interconnecting = configuration.GetConnectionString("DefaultConnection");
            builder.UseMySql(interconnecting);
            return new AppDbContext(builder.Options);
        }
    }
}