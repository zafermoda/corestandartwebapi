using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoreStandartWebapi.Data
{
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        public string CategoryTitle { get; set; }
        public string CategoryDescription { get; set; }
        
        public List<Product> Products { get; set; }
    }


}